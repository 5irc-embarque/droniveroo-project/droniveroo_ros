# Droniveroo

> Projet de fin d'année de la majeur embarqué

Le but du projet est la livraison de colis à l'aide d'un drone.
Le projet est découpé en 3 répertoires:   
- [ROS](https://gitlab.com/5irc-embarque/droniveroo-project/droniveroo_ros) (pour le turtlebot et le drone) 
- [Boite](https://gitlab.com/5irc-embarque/droniveroo-project/droniveroo_camera)
- [Serveur Web](https://gitlab.com/5irc-embarque/droniveroo-project/droniveroo_web)


## Architecture du repo
Cette branche du projet utilise le projet zbar pour la lecture de QR Code (le package [zbar_ros](http://wiki.ros.org/zbar_ros)). Le repo est découpé en 4 modules:
- droniveroo_drone : 
    - navigation du drone
    - lecture QRCode
    - `move.py` : navigation du drone
    - `control.py` : controle du drone à l'aide d'une manette
    - `altitude.py` : non utilisé
- droniveroo_msgs :
    - contient un message pour pozyx
        - uint32 x
        - uint32 y
        - uint32 z
        - bool error
- droniveroo_pozyx :
    - gestion de pozyx
    - 4 topics:
        - pozyx/position/1
        - pozyx/position/2
        - pozyx/position/avg/1
        - pozyx/position/avg/2
    - `position.py` : Le script gère plusieurs tags. Il faut utiliser la méthode `addTag` d'un objet `PozyxSerial`
- dronivero_turtle : 
    - navigation du drone
    - lecture QRCode
- droniveroo_box
    - paquet non utilisé

## Spécificité de repo drone
Les fonctionnalités sont sur différentes branches.

### Turtlebot

Sur `feature/turtle_mainBehavior` se trouve tout le workflow du turtlebot. Il gère la lecture du QRCode, la navigation du turtlebot et les appels au serveur web.

#### Démarrage

Le démarrage du workflow doit se faire directement sur l'ordinateur de contrôle du turtlebot (via SSH par exemple). Le `roscore` doit d'abord être démarré. Lancer ensuite le launcher `main.launch` du package `droniveroo_turtle`.

### Drone

Sur `feature/move-drone` se trouve le code de navigation du drone
> Le commit `df23cd4bb4a3eed9b0de0ab396add5fb022308ae` (land after move) contient la finalité du premier algorithme (calcul des angles)  
> Le dernier commit de cette branche contient le deuxième algorithme (composition de vecteurs)

Pour les deux algorithme le launcher droniveroo_drone move.launch lance tout le nécessaire pour tester:
- node bebop
- joy
- controle de la manette
- pozyx
- navigation du drone