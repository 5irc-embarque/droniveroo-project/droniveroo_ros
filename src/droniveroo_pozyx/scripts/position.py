#!/usr/bin/env python3

import rospy
from std_msgs.msg import String
# from bebop_msgs.msg import Ardrone3PilotingStateAltitudeChanged
from time import sleep
from droniveroo_msgs.msg import Position

from pypozyx import (POZYX_POS_ALG_UWB_ONLY, POZYX_3D, Coordinates, POZYX_SUCCESS, PozyxConstants, version,
                     DeviceCoordinates, PozyxSerial, get_first_pozyx_serial_port, SingleRegister, DeviceList, PozyxRegisters)

from pypozyx.tools.version_check import perform_latest_version_check
from collections import deque


class PozyxPosition(object):
    """Continuously calls the Pozyx positioning function and prints its position."""

    def __init__(self, pozyx, anchors, algorithm=POZYX_POS_ALG_UWB_ONLY, dimension=POZYX_3D, height=1000, remote_id=None):
        self.pozyx = pozyx
        self.osc_udp_client = None

        self.anchors = anchors
        self.algorithm = algorithm
        self.dimension = dimension
        self.height = height
        self.tags = [remote_id]
        self.tagsPublish = {
            remote_id: rospy.Publisher("pozyx/position/1", Position, queue_size=10)
        }
        self.tagsAvgPublish = {
            remote_id: rospy.Publisher("pozyx/position/avg/1", Position, queue_size=10)
        }
        self.positions = {
            remote_id: Position(0, 0, 0, True)
        }
        self.positionsTime = {
            remote_id: deque([])
        }

    def addTag(self, remote_id=None):
        self.tags.append(remote_id)
        self.tagsPublish[remote_id] = rospy.Publisher(
            "pozyx/position/" + str(len(self.tags)), Position, queue_size=10)
        self.tagsAvgPublish[remote_id] = rospy.Publisher(
            "pozyx/position/avg/" + str(len(self.tags)), Position, queue_size=10)
        self.positions[remote_id] = Position(0, 0, 0, True)
        self.positionsTime[remote_id] = deque([])

    def setup(self):
        """Sets up the Pozyx for positioning by calibrating its anchor list."""
        print("------------POZYX POSITIONING V{} -------------".format(version))
        print("")
        print("- System will manually configure tag")
        print("")
        print("- System will auto start positioning")
        print("")
        print("Tags :")
        for device in self.tags:
            self.pozyx.printDeviceInfo(device)
        print("Anchors :")
        for device in self.anchors:
            self.pozyx.printDeviceInfo(device.network_id)
        print("")
        print("------------POZYX POSITIONING V{} -------------".format(version))
        print("")

        self.setAnchorsManual(save_to_flash=False)
        # self.printPublishConfigurationResult()

    def loop(self):
        """Performs positioning and displays/exports the results."""
        position = Coordinates()
        print("search positioning")
        for remote_id in self.tags:
            status = self.pozyx.doPositioning(
                position, self.dimension, self.height, self.algorithm, remote_id=remote_id)
            if status == POZYX_SUCCESS:
                self.printPublishPosition(remote_id, position)
                error = position.x == 0 and position.y == 0 and position.z == 0
                self.positions[remote_id] = Position(
                    abs(position.x), abs(position.y), abs(position.z), error)
            else:
                self.printPublishErrorCode(remote_id, "positioning")
                self.positions[remote_id] = Position(
                    abs(position.x), abs(position.y), abs(position.z), True)
            if self.positions[remote_id].error == False:
                self.positionsTime[remote_id].append(self.positions[remote_id])
                if len(self.positionsTime[remote_id]) > 4:
                    self.positionsTime[remote_id].popleft()
            # Remove values to update avg
            else:
                if len(self.positionsTime[remote_id]) != 0:
                    self.positionsTime[remote_id].popleft()
        return self.positions

    def publishTagPosition(self):
        for remote_id in self.tags:
            # Publish position
            self.tagsPublish[remote_id].publish(self.positions[remote_id])

            # Publish AVG
            if len(self.positionsTime[remote_id]) != 0:
                posAvg = Position(0, 0, 0, False)
                for position in self.positionsTime[remote_id]:
                    posAvg.x += position.x
                    posAvg.y += position.y
                    posAvg.z += position.z
                posAvg.x = int(posAvg.x / len(self.positionsTime[remote_id]))
                posAvg.y = int(posAvg.y / len(self.positionsTime[remote_id]))
                posAvg.z = int(posAvg.z / len(self.positionsTime[remote_id]))
                self.tagsAvgPublish[remote_id].publish(posAvg)
            else:
                self.tagsAvgPublish[remote_id].publish(Position(0, 0, 0, True))

    def printPublishPosition(self, remote_id,  position):
        """Prints the Pozyx's position and possibly sends it as a OSC packet"""
        network_id = remote_id
        if network_id is None:
            network_id = 0
        print("POS ID {}, x(mm): {pos.x} y(mm): {pos.y} z(mm): {pos.z}".format(
            "0x%0.4x" % network_id, pos=position))
        if self.osc_udp_client is not None:
            self.osc_udp_client.send_message(
                "/position", [network_id, int(position.x), int(position.y), int(position.z)])

    def printPublishErrorCode(self, remote_id, operation):
        """Prints the Pozyx's error and possibly sends it as a OSC packet"""
        error_code = SingleRegister()
        network_id = remote_id
        if network_id is None:
            self.pozyx.getErrorCode(error_code)
            print("LOCAL ERROR %s, %s" %
                  (operation, self.pozyx.getErrorMessage(error_code)))
            if self.osc_udp_client is not None:
                self.osc_udp_client.send_message(
                    "/error", [operation, 0, error_code[0]])
            return
        status = self.pozyx.getErrorCode(error_code, network_id)
        if status == POZYX_SUCCESS:
            print("ERROR %s on ID %s, %s" %
                  (operation, "0x%0.4x" % network_id, self.pozyx.getErrorMessage(error_code)))
            if self.osc_udp_client is not None:
                self.osc_udp_client.send_message(
                    "/error", [operation, network_id, error_code[0]])
        else:
            self.pozyx.getErrorCode(error_code)
            print("ERROR %s, couldn't retrieve remote error code, LOCAL ERROR %s" %
                  (operation, self.pozyx.getErrorMessage(error_code)))
            if self.osc_udp_client is not None:
                self.osc_udp_client.send_message("/error", [operation, 0, -1])
            # should only happen when not being able to communicate with a remote Pozyx.

    def setAnchorsManual(self, save_to_flash=False):
        """Adds the manually measured anchors to the Pozyx's device list one for one."""
        status = True
        for remote_id in self.tags:
            status &= self.pozyx.clearDevices(remote_id=remote_id)
            for anchor in self.anchors:
                status &= self.pozyx.addDevice(anchor, remote_id=remote_id)
            if len(self.anchors) > 4:
                status &= self.pozyx.setSelectionOfAnchors(PozyxConstants.ANCHOR_SELECT_AUTO, len(self.anchors),
                                                           remote_id=remote_id)

            if save_to_flash:
                self.pozyx.saveAnchorIds(remote_id=remote_id)
                self.pozyx.saveRegisters(
                    [PozyxRegisters.POSITIONING_NUMBER_OF_ANCHORS], remote_id=remote_id)
        return status

    def printPublishConfigurationResult(self):
        """Prints and potentially publishes the anchor configuration result in a human-readable way."""
        list_size = SingleRegister()

        for remote_id in self.tags:
            self.pozyx.getDeviceListSize(list_size, remote_id)
            print("List size: {0}".format(list_size[0]))
            if list_size[0] != len(self.anchors):
                self.printPublishErrorCode(remote_id, "configuration")
                return
            device_list = DeviceList(list_size=list_size[0])
            self.pozyx.getDeviceIds(device_list, remote_id)
            print("Calibration result:")
            print("Anchors found: {0}".format(list_size[0]))
            print("Anchor IDs: ", device_list)

            for i in range(list_size[0]):
                anchor_coordinates = Coordinates()
                self.pozyx.getDeviceCoordinates(
                    device_list[i], anchor_coordinates, remote_id)
                print("ANCHOR, 0x%0.4x, %s" %
                    (device_list[i], str(anchor_coordinates)))
                if self.osc_udp_client is not None:
                    self.osc_udp_client.send_message(
                        "/anchor", [device_list[i], int(anchor_coordinates.x), int(anchor_coordinates.y), int(anchor_coordinates.z)])
                    sleep(0.025)

    def printPublishAnchorConfiguration(self):
        """Prints and potentially publishes the anchor configuration"""
        for anchor in self.anchors:
            print("ANCHOR,0x%0.4x,%s" %
                  (anchor.network_id, str(anchor.coordinates)))
            if self.osc_udp_client is not None:
                self.osc_udp_client.send_message(
                    "/anchor", [anchor.network_id, int(anchor.coordinates.x), int(anchor.coordinates.y), int(anchor.coordinates.z)])
                sleep(0.025)


class postion:
    def __init__(self):
        # PORT = '/dev/ttyACM0'
        rospy.init_node('pozyx_position', anonymous=True)
        # self.publishPos = rospy.Publisher("pozyx/position", Position, queue_size=10)
        self.publishPosAvg = rospy.Publisher(
            "pozyx/position/avg", Position, queue_size=10)
        self.rate = rospy.Rate(10)
        self.positions = deque([])

        check_pypozyx_version = True
        if check_pypozyx_version:
            perform_latest_version_check()

        # shortcut to not have to find out the port yourself
        serial_port = get_first_pozyx_serial_port()

        if serial_port is None:
            print("No Pozyx connected. Check your USB cable or your driver!")
            quit()

        remote_id = 0x606B               # remote device network ID
        remote = False                   # whether to use a remote device
        if not remote:
            remote_id = None

        # necessary data for calibration, change the IDs and coordinates yourself according to your measurement
        anchors = [DeviceCoordinates(0x603B, 1, Coordinates(0, 0, 0)),
                   DeviceCoordinates(0x603A, 1, Coordinates(2390, 5460, 0)),
                   DeviceCoordinates(0x6037, 1, Coordinates(6560, 0, 1020)),
                   DeviceCoordinates(0x683D, 1, Coordinates(0, 4340, 0))]

        # positioning algorithm to use, other is PozyxConstants.POSITIONING_ALGORITHM_TRACKING
        algorithm = PozyxConstants.POSITIONING_ALGORITHM_UWB_ONLY
        # positioning dimension. Others are PozyxConstants.DIMENSION_2D, PozyxConstants.DIMENSION_2_5D
        dimension = PozyxConstants.DIMENSION_3D
        # height of device, required in 2.5D positioning
        height = 1000

        pozyx = PozyxSerial(serial_port)
        r = PozyxPosition(pozyx, anchors, algorithm,
                          dimension, height, remote_id)
        r.addTag(0x6F4A)
        r.setup()

        while not rospy.is_shutdown():
            r.loop()
            r.publishTagPosition()
            self.rate.sleep()


if __name__ == '__main__':
    postion()
