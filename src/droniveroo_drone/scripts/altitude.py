#!/usr/bin/env python

import rospy
from std_msgs.msg import String
from bebop_msgs.msg import Ardrone3PilotingStateAltitudeChanged

class altitude:

    def __init__(self):
        self.pub = rospy.Publisher("altitude", String, queue_size=10)
        rospy.init_node('altitude', anonymous=True)
        rospy.Subscriber("/bebop/states/ardrone3/PilotingState/AltitudeChanged", Ardrone3PilotingStateAltitudeChanged, self.listenCallback)
        rospy.spin()

    def listenCallback(self,data):
        rospy.loginfo(rospy.get_caller_id()+"I heard %s",data.altitude)
        self.pub.publish(str(data.altitude))



if __name__ == '__main__':
    altitude()