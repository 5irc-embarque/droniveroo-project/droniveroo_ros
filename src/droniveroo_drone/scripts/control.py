#!/usr/bin/env python

import rospy
from std_msgs.msg import String
from std_msgs.msg import Empty
from std_msgs.msg import UInt8
from geometry_msgs.msg import Twist, Vector3
from sensor_msgs.msg import Joy
from mini_projet.srv import *

class control:

    def __init__(self):
        self.pilot_cmd = rospy.Publisher("/control/bebop/cmd_vel", Twist, queue_size=0)
        self.takeoff = rospy.Publisher("/control/bebop/takeoff", Empty, queue_size=10)
        self.land = rospy.Publisher("/control/bebop/land", Empty, queue_size=10)
        self.flip = rospy.Publisher("/control/bebop/flip", UInt8, queue_size=10)
        self.reset = rospy.Publisher("/control/bebop/reset", Empty, queue_size=10)

        turretService = rospy.Service('turret_service', Turret, self.handleTurretMode)
        touristService = rospy.Service('tourist_service', Tourist, self.handleTouristMode)

        rospy.init_node('control', anonymous=True)
        rospy.Subscriber("/control/joy", Joy, self.listenCallback)
        self.rate = rospy.Rate(10)
        self.y1 = 0
        self.y2 = 0
        self.x1 = 0
        self.x2 = 0
        self.surveillance = False
        while not rospy.is_shutdown():
            if self.surveillance:
                self.y1 = 0
                self.y2 = 0
                self.x1 = 0
                self.x2 = 0.5
            self.sendVelocity()
            self.rate.sleep()
        # rospy.spin()

    def listenCallback(self,data):
        # rospy.loginfo(rospy.get_caller_id()+"Buttons :  %s | Axes : %s |",str(data.buttons), str(data.axes))
        # rospy.loginfo(rospy.get_caller_id()+"Button1 : %d",data.buttons[0])
        if data.buttons[9]:
            rospy.loginfo("BOUTON 10")
            self.takeoff.publish()
        elif data.buttons[8]:
            rospy.loginfo("BOUTON 9")
            self.land.publish()
        elif data.buttons[0]:
            rospy.loginfo("BOUTON 1")
            self.reset.publish()
        elif data.buttons[2]:
            if self.surveillance:
                self.surveillance = False
            else:
                self.surveillance = True
        elif not self.surveillance:
            if data.axes[6] == 1:
                rospy.loginfo("AXES UP")
                self.flip.publish(1)
            elif data.axes[6] == -1:
                rospy.loginfo("AXES DOWN")
                self.flip.publish(0)
            elif data.axes[5] == 1:
                rospy.loginfo("AXES LEFT")
                self.flip.publish(3)
            elif data.axes[5] == -1:
                rospy.loginfo("AXES RIGHT")
                self.flip.publish(2)
            else:
                self.y1 = data.axes[1]
                self.y2 = data.axes[4]
                self.x1 = data.axes[0]
                self.x2 = data.axes[3]
                # self.sendVelocity()

    def sendVelocity(self):
        trans = Vector3(self.y1,self.x1,self.y2)
        ang = Vector3(0,0,self.x2)
        cmd = Twist(trans, ang)
        self.pilot_cmd.publish(cmd)

    def handleTurretMode(self, data):
        self.surveillance = True
        return True

    def handleTouristMode(self, data):
        self.surveillance = False
        return True



if __name__ == '__main__':
    control()