#!/usr/bin/env python3

import rospy
from geometry_msgs.msg import (Twist, Vector3)
from std_msgs.msg import String
from droniveroo_msgs.msg import Position
from nav_msgs.msg import Odometry

from pypozyx import (POZYX_POS_ALG_UWB_ONLY, POZYX_3D, Coordinates, POZYX_SUCCESS, PozyxConstants, version,
                     DeviceCoordinates, PozyxSerial, get_first_pozyx_serial_port, SingleRegister, DeviceList, PozyxRegisters)

from pypozyx.tools.version_check import perform_latest_version_check
from collections import deque

import numpy as np


class Calibration:
    def __init__(self):
        self.calibrationPosStart = Vector3(0, 0, 0)
        self.calibrationPosEnd = Vector3(0, 0, 0)
        self.step = 0
        self.stepStart = 0
        self.stepStop = 3
        # self.stepCheckDist = 4

    def setCallibrationStart(self, start):
        self.calibrationPosStart = start

    def setCallibrationEnd(self, end):
        self.calibrationPosEnd = end

    def checkDist(self, finalX, finalY):

        angle = self.angle_between((self.calibrationPosEnd.x - self.calibrationPosStart.x, self.calibrationPosEnd.y - self.calibrationPosStart.y),
                                   (finalX - self.calibrationPosEnd.x, finalY - self.calibrationPosEnd.y))
        rospy.loginfo(rospy.get_caller_id() + " / angle : %f", angle)
        return angle

    """ Returns the unit vector of the vector.  """

    def unit_vector(self, vector):
        return vector / np.linalg.norm(vector)

    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """

    def angle_between(self, v1, v2):

        v1_u = self.unit_vector(v1)
        v2_u = self.unit_vector(v2)
        print(v1)
        print(v2)
        return np.arctan2(v2_u[1], v2_u[0]) - np.arctan2(v1_u[1], v1_u[0])


class TurtleDirection:
    def __init__(self):
        self.calibrationPos = []
        self.step = 0
        self.stepDoCalibration = 5
        self.stepStart = 0


class move:

    def __init__(self):
        rospy.init_node('move', anonymous=True)
        self.xFinal = 2100
        self.yFinal = 2900
        self.calibration = Calibration()
        self.turtleDirection = TurtleDirection()
        self.calibrationPhase = True
        self.centered = False
        self.centeredOnOneAxe = False
        self.realAngularSpeed = 0

        self.move = Vector3(0, 0, 0)

        # Publisher for commanding trutle
        self.publisherMove = rospy.Publisher('/mobile_base/commands/velocity', Twist, queue_size=10)
        self.odom = rospy.Subscriber('/odom', Odometry, self.odomCallback)
        self.rate = rospy.Rate(50)

        # Subscriber for getting turtle pos
        self.getPosition = rospy.Subscriber("pozyx/position/avg/2", Position, self.positionCallback)
        # Subscriber for getting final pos
        self.getPositionObjectif = rospy.Subscriber("pozyx/position/avg/1", Position, self.positionObjectifCallback)

        rospy.spin()

    def positionObjectifCallback(self, data):
        if(data.x != 0 and data.y != 0 and data.z != 0 and data.error == False):
            self.xFinal = data.x
            self.yFinal = data.y
            rospy.loginfo(" final : x : %i, y: %i", self.xFinal, self.yFinal)

    def odomCallback(self, data):
        self.realAngularSpeed = data.twist.twist.angular.z

    def positionCallback(self, data):
        pos = Vector3(data.x, data.y, data.z)
        rospy.loginfo(rospy.get_caller_id() + " / x : %i, y : %i, z : %i", pos.x, pos.y, pos.z)
        # Calibration phase
        if(self.calibrationPhase):
            if(pos.x != 0 and pos.y != 0 and pos.z != 0 and data.error == False):
                # Start
                if(self.calibration.stepStart == 0):
                    rospy.loginfo(rospy.get_caller_id() + " / Step 0")
                    self.calibration.calibrationPosStart = pos
                    self.calibration.stepStart = rospy.Time.now().to_sec()
                if(rospy.Time.now().to_sec() - self.calibration.stepStart < self.calibration.stepStop):
                    self.publisherMove.publish(Twist(Vector3(0.2, 0, 0), Vector3(0, 0, 0)))
                # check distance
                else:
                    rospy.loginfo(rospy.get_caller_id() + " / Step stepCheckDist")
                    self.publisherMove.publish(Twist(Vector3(0, 0, 0), Vector3(0, 0, 0)))
                    self.calibration.calibrationPosEnd = pos
                    rospy.loginfo(rospy.get_caller_id() + " /Start : x : %i, y : %i, z : %i",
                                  self.calibration.calibrationPosStart.x, self.calibration.calibrationPosStart.y, self.calibration.calibrationPosStart.z)
                    rospy.loginfo(rospy.get_caller_id() + " /End : x : %i, y : %i, z : %i",
                                  self.calibration.calibrationPosEnd.x, self.calibration.calibrationPosEnd.y, self.calibration.calibrationPosEnd.z)
                    rospy.loginfo(rospy.get_caller_id() + " /calcul xFinal : %i and yFinal : %i", self.xFinal, self.yFinal)                    
                    angle = self.calibration.checkDist(self.xFinal, self.yFinal)
                    self.calibration.calibrationPosStart = self.calibration.calibrationPosEnd

                    if(abs(angle) > 2):
                        angular_speed = 90*2*3.1415926535897/360
                    else:
                        angular_speed = 60*2*3.1415926535897/360
                    if(angle < 0):
                        angular_speed = -angular_speed
                    # Setting the current time for distance calculus
                    t0 = rospy.Time.now().to_nsec()
                    current_angle = 0
                    while(current_angle < abs(angle)):
                        self.publisherMove.publish(Twist(Vector3(0, 0, 0), Vector3(0, 0, angular_speed)))
                        t1 = rospy.Time.now().to_nsec()
                        traveled_angle = self.realAngularSpeed*((t1-t0)/1000000000)
                        current_angle += abs(traveled_angle)
                        t0 = t1
                        self.rate.sleep()
                    rospy.loginfo("current_angle : %f", current_angle)
                    # Stop
                    self.publisherMove.publish(Twist(Vector3(0, 0, 0), Vector3(0, 0, 0)))
                    self.calibrationPhase = False
                    self.turtleDirection.stepStart = rospy.Time.now().to_sec()
        # Not Centered yet
        elif(not self.centered):
            rospy.loginfo(rospy.get_caller_id() + '/ run phase')
            # No error
            if(pos.x != 0 and pos.y != 0 and pos.z != 0 and data.error == False):
                # Too long to find something
                if (rospy.Time.now().to_sec() - self.turtleDirection.stepStart > self.turtleDirection.stepDoCalibration):
                    rospy.loginfo('Too long to find something')
                    self.move.x = 0
                    self.recalibrate()
                # Center
                elif (pos.y >= self.yFinal - 100 and pos.y <= self.yFinal + 100 and pos.x >= self.xFinal - 100 and pos.x <= self.xFinal + 100):
                    rospy.loginfo('Center')
                    self.move.x = 0
                    self.centered = True
                else:
                    self.move.x = 0.2
            # Do nothing because of error
            else:
                rospy.loginfo(rospy.get_caller_id() + '0')
                self.move.x = 0
            rospy.loginfo(rospy.get_caller_id() + " / move: %i", self.move.x)
            data = Twist(self.move, Vector3(0, 0, 0))
            self.publisherMove.publish(data)
            self.turtleDirection.step += 1
        # Centered
        else:
            rospy.loginfo(rospy.get_caller_id() + '/ I am centered :O :) :D')
            self.getPosition.unregister()
            self.getPositionObjectif.unregister()

    def recalibrate(self):
        self.turtleDirection.step = 0
        self.calibrationPhase = True
        self.calibration.step = self.calibration.stepStop


if __name__ == '__main__':
    move()
